;; -*- geiser-scheme-implementation: chicken -*-

;;; The Little Schemer, Fourth Edition

;; (load "tls.scm")

;;; 1. Toys

(define atom?
  (lambda (x)
    (and (not (pair? x)) (not (null? x)))))

;;; 2. Do It, Do It Again, and Again, and Again...

(define lat?  ; list of atoms?
  (lambda (l)
    (cond
     ((null? l) #t)
     ((atom? (car l) (lat? (cdr l))))
     (else #f))))

(define member?
  (lambda (a lat)
    (cond
     ((null? lat) #f)
     (else (or (eq? (car lat) a)
               (member? a (cdr lat)))))))

;;               The First Commandment
;;                   (preliminary)
;;
;; Always ask null? as the first question in expressing
;; any function.

;;; 3. Cons the Magnificent

;;  The Second Commandment
;;
;; Use cons to build lists.

(define rember  ; remove a member
  (lambda (a lat)
    (cond
     ((null? lat) (quote ()))
     ((eq? (car lat) a) (cdr lat))
     (else (cons (car lat) (rember a (cdr lat)))))))

;;               The Third Commandment
;;
;; When building a list, describe the first typical ele-
;; ment, and then cons it onto the natural recursion.

(define firsts
  (lambda (l)
    (cond
     ((null? l) (quote ()))
     (else (cons (car (car l)) (firsts (cdr l)))))))

(define insertR
  (lambda (new old lat)
    (cond
     ((null? lat) (quote ()))
     (else
      (cond
       ((eq? (car lat) old) (cons old (cons new (cdr lat))))
       (else (cons (car lat) (insertR new old (cdr lat)))))))))

(define insertL
  (lambda (new old lat)
    (cond
     ((null? lat) (quote ()))
     (else
      (cond
       ((eq? (car lat) old) (cons new lat))
       (else (cons (car lat) (insertL new old (cdr lat)))))))))

(define subst
  (lambda (new old lat)
    (cond
     ((null? lat) (quote ()))
     (else
      (cond
       ((eq? (car lat) old) (cons new (cdr lat)))
       (else (cons (car lat) (subst new old (cdr lat)))))))))

(define subst2
  (lambda (new o1 o2 lat)
    (cond
     ((null? lat) (quote ()))
     (else
      (cond
       ((or (eq? (car lat) o1) (eq? (car lat) o2)) (cons new (cdr lat)))
       (else (cons (car lat) (subst2 new o1 o2 (cdr lat)))))))))

(define multirember
  (lambda (a lat)
    (cond
     ((null? lat) (quote ()))
     (else
      (cond
       ((eq? (car lat) a) (multirember a (cdr lat)))
       (else (cons (car lat) (multirember a (cdr lat)))))))))

(define multiinsertR
  (lambda (new old lat)
    (cond
     ((null? lat) (quote ()))
     (else
      (cond
       ((eq? (car lat) old)
        (cons old (cons new (multiinsertR new old (cdr lat)))))
       (else (cons (car lat) (multiinsertR new old (cdr lat)))))))))

(define multiinsertL
  (lambda (new old lat)
    (cond
     ((null? lat) (quote ()))
     (else
      (cond
       ((eq? (car lat) old)
        (cons new (cons old (multiinsertL new old (cdr lat)))))
       (else (cons (car lat) (multiinsertL new old (cdr lat)))))))))

;;                 The Fourth Commandment
;;                      (preliminary)
;;
;; Always change at least one argument while recurring.  It
;; must be changed to be closer to termination.  The changing
;; argument must be tested in the termination condition:
;; when using cdr, test termination with null?.

(define multisubst
  (lambda (new old lat)
    (cond
     ((null? lat) (quote ()))
     (else
      (cond
       ((eq? (car lat) old) (cons new (cdr lat)))
       (else (cons (car lat) (multisubst new old (cdr lat)))))))))

;;; 4. Numbers Games

(define o+
  (lambda (n m)
    (cond
     ((zero? m) n)
     (else (add1 (o+ n (sub1 m)))))))

(define o-
  (lambda (n m)
    (cond
     ((zero? m) n)
     (else (sub1 (o- n (sub1 m)))))))

;;                 The First Commandment
;;                   (first revision)
;;
;; When recurring on a list of atoms, lat, ask two questions
;; about it: (null? lat) and else.
;; When recurring on a number, n, ask two question about
;; it: (zero? n) and else.

(define addtup  ; add tuple
  (lambda (tup)
    (cond
     ((zero? tup) 0)
     (else (o+ (car tup) (addtup (cdr tup)))))))

;;                 The Fourth Commandment
;;                    (first revision)
;;
;; Always change at least one argument while recurring.  It
;; must be changed to be closer to termination.  The changing
;; argument must be tested in the termination condition:
;;
;; when using cdr, test termination with null? and
;; when using sub1, tst termination with zero?.

(define x
  (lambda (n m)
    (cond
     ((zero? m) 0)
     (else (o+ n (x n (sub1 m)))))))

;;                      The Fifth Commandment
;;
;; When building a value with o+, always use 0 as the value of the
;; terminating line, for adding 0 does not change the value of an
;; addition.
;;
;; When building a value with x, always use 1 for the value of the
;; terminating line, for multiplying by 1 does not change the value
;; of a multiplication.
;;
;; When building a value with cons, always consider () for the value
;; of the terminating line.

(define tup+
  (lambda (tup1 tup2)
    (cond
     ((null? tup1) tup2)
     ((null? tup2) tup1)
     (else (cons (o+ (car tup1) (car tup2)) (tup+ (cdr tup1) (cdr tup2)))))))

(define >
  (lambda (n m)
    (cond
     ((zero? n) #f)
     ((zero? m) #t)
     (else (> (sub1 n) (sub1 m))))))

(define <
  (lambda (n m)
    (cond
     ((zero? m) #f)
     ((zero? n) #t)
     (else (< (sub1 n) (sub1 m))))))

(define =
  (lambda (n m)
    (cond
     ((> n m) #f)
     ((< n m) #f)
     (else #t))))

(define ^
  (lambda (n m)
    (cond
     ((zero? m) 1)
     (else (x n (^ n (sub1 m)))))))

(define /
  (lambda (n m)
    (cond
     ((< n m) 0)
     (else (add1 (/ (o- n m) m))))))

(define length
  (lambda (lat)
    (cond
     ((null? lat) 0)
     (else (add1 (length (cdr lat)))))))

(define pick
  (lambda (n lat)
    (cond
     ((zero? (sub1 n)) (car lat))
     (else (pick (sub1 n) (cdr lat))))))

(define rempick
  (lambda (n lat)
    (cond
     ((zero? (sub1 n)) (cdr lat))
     (else (cons (car lat) (rempick (sub1 n) (cdr lat)))))))

(define no-nums
  (lambda (lat)
    (cond
     ((null? lat) (quote ()))
     (else
      (cond
       ((number? (car lat)) (no-nums (cdr lat)))
       (else (cons (car lat) (no-nums (cdr lat)))))))))

(define all-nums
  (lambda (lat)
    (cond
     ((null? lat) (quote ()))
     (else
      (cond
       ((number? (car lat)) (cons (car lat) (all-nums (cdr lat))))
       (else (all-nums (cdr lat))))))))

(define eqan?
  (lambda (a1 a2)
    (cond
     ((and (number? a1) (number? a2)) (= a1 a2))
     ((or (number? a1) (number? a2)) #f)
     (else (eq? a1 a2)))))

(define occur
  (lambda (a lat)
    (cond
     ((null? lat) 0)
     (else
      (cond
       ((eq? (car lat) a) (add1 (occur a (cdr lat))))
       (else (occur a (cdr lat))))))))

(define one?
  (lambda (n)
    (= n 1)))

(define rempick
  (lambda (n lat)
    (cond
     ((one? n) (cdr lat))
     (else (cons (car lat) (rempick (sub1 n) (cdr lat)))))))

;;; 5. *Oh My Gawd*: It's Full of Stars

(define rember*
  (lambda (a l)
    (cond
     ((null? l) (quote ()))
     ((atom? (car l))
      (cond
       ((eq? (car l) a) (rember* a (cdr l)))
       (else (cons (car l) (rember* a (cdr l))))))
     (else (cons (rember* a (car l)) (rember* a (cdr l)))))))

(define insertR*
  (lambda (new old l)
    (cond
     ((null? l) (quote ()))
     ((atom? (car l))
      (cond
       ((eq? (car l) old) (cons old (cons new (insertR* new old (cdr l)))))
       (else (cons (car l) (insertR* new old (cdr l))))))
     (else (cons (insertR* new old (car l)) (insertR* new old (cdr l)))))))

;;                 The First Commandment
;;                    (final version)
;;
;; When recurring on a list of atoms, lat, ask two questions
;; about it: (null? lat) and else.
;; When recurring on a number, n, ask two questions about
;; it: (zero? n) and else.
;; When recurring on a list of S-expressions, l, ask three
;; question about it: (null? l), (atom? (car l)), and else.

;;                   The Fourth Commandment
;;                      (final version)
;;
;; Always change at least one argument while recurring.
;; When recurring on a list of atoms, lat, use (cdr  lat).  When
;; recurring on a number, n, use (sub1 n).  And when recur-
;; ring on a list of S-expressions, l, use (car l) and (cdr l) if
;; neither (null? l) nor (atom? (car l)) are true.
;;
;; It must be changed to be closer to termination.  The chang-
;; ing argument must be tested in the termination condition:
;;
;; when using cdr, test termination with null? and
;; when using sub1, test termination with zero?.

(define occur*
  (lambda (a l)
    (cond
     ((null? l) 0)
     (else
      (cond
       ((atom? (car l))
        (cond
         ((eq? (car l) a) (add1 (occur* a (cdr l))))
         (else (occur* a (cdr l)))))
       (else (o+ (occur* a (car l)) (occur* a (cdr l)))))))))

(define subst*
  (lambda (new old l)
    (cond
     ((null? l) (quote ()))
     (else
      (cond
       ((atom? (car l))
        (cond
         ((eq? (car l) old) (cons new (subst* new old (cdr l))))
         (else (cons (car l) (subst* new old (cdr l))))))
       (else (cons (subst* new old (car l)) (subst* new old (cdr l)))))))))

(define insertL*
  (lambda (new old l)
    (cond
     ((null? l) (quote ()))
     (else
      (cond
       ((atom? (car l))
        (cond
         ((eq? (car l) old) (cons new (cons old (insertL* new old (cdr l)))))
         (else (cons (car l) (insertL* new old (cdr l))))))
       (else (cons (insertL* new old (car l)) (insertL* new old (cdr l)))))))))

(define member*
  (lambda (a l)
    (cond
     ((null? l) #f)
     (else
      (cond
       ((atom? (car l))
        (cond
         ((eq? (car l) a) #t)
         (else (member* a (cdr l)))))
       (else (or (member* a (car l)) (member* a (cdr l)))))))))

(define leftmost
  (lambda (l)
    (cond
     ((atom? (car l)) (car l))
     (else (leftmost (car l))))))

(define eqlist?
  (lambda (l1 l2)
    (cond
     ((and (null? l1) (null? l2)) #t)
     ((or (null? l1) (null? l2)) #f)
     ((and (atom? (car l1)) (atom? (car l2)))
      (and ((eqan? (car l1) (car l2)) (eqlist? (cdr l1) (cdr l2)))))
     ((or (atom? (car l1)) (atom? (car l2))) #f)
     ((and (eqlist? (car l1) (car l2)) (eqlist? (cdr l1) (cdr l2)))))))

(define equal?
  (lambda (s1 s2)
    (cond
     ((and (atom? s1) (atom? s2)) (eqan? s1 s2))
     ((or (atom? s1) (atom? s2)) #f)
     (else (eqlist? s1 s2)))))

(define eqlist?
  (lambda (l1 l2)
    (cond
     ((and (null? l1) (null? l2) #t))
     ((or (null? l1) (null? l2)) #f)
     (else (and (equal? (car l1) (car l2)) (eqlist? (cdr l1) (cdr l2)))))))

;;           The Sixth Commandment
;;
;; Simplify only after the function is correct.

(define rember
  (lambda (s l)
    (cond
     ((null? l) (quote ()))
     ((equal? (car l) s) (cdr l))
     (else (cons (car l) (rember s (cdr l)))))))

;;; 6. Shadows

(define numbered?
  (lambda (aexp)
    (cond
     ((atom? aexp) (number? aexp))
     ((eq? (car (cdr aexp)) (quote +))
      (and (numbered? (car aexp))
           (numbered? (car (cdr (cdr aexp))))))
     ((eq? (car (cdr aexp)) (quote x))
      (and (numbered? (car aexp))
           (numbered? (car (cdr (cdr aexp))))))
     ((eq? (car (cdr aexp)) (quote ^))
      (and (numbered? (car aexp))
           (numbered? (car (cdr (cdr aexp)))))))))

(define numbered?
  (lambda (aexp)
    (cond
     ((atom? aexp) (number? aexp))
     (else
      (and (numbered? (car aexp))
           (numbered? (car (cdr (cdr aexp)))))))))

;;              The Seventh Commandment
;;
;; Recur on the subparts that are of the same nature:
;; * On the sublists of a list.
;; * On the subexpressions of an arithmetic expression.

(define value
  (lambda (nexp)
    (cond
     ((atom? nexp) nexp)
     ((eq? (car (cdr nexp)) (quote +))
      (o+ (value (car nexp))
          (value (car (cdr (cdr nexp))))))
     ((eq? (car (cdr nexp)) (quote x))
      (x (value (car nexp))
         (value (car (cdr (cdr nexp))))))
     (else
      (^ (value (car nexp))
         (value (car (cdr (cdr nexp)))))))))

(define 1st-sub-exp
  (lambda (aexp)
    (car (cdr aexp))))

(define 2nd-sub-exp
  (lambda (aexp)
    (car (cdr (cdr aexp)))))

(define operator
  (lambda (aexp)
    (car aexp)))

(define value
  (lambda (nexp)
    (cond
     ((atom? nexp) nexp)
     ((eq? (operator nexp) (quote +))
      (o+ (value (1st-sub-exp nexp))
          (value (2nd-sub-exp nexp))))
     ((eq? (operator nexp) (quote x))
      (x (value (1st-sub-exp nexp))
         (value (2nd-sub-exp nexp))))
     (else
      (^ (value (1st-sub-exp nexp))
         (value (2nd-sub-exp nexp)))))))

(define 1st-sub-exp
  (lambda (aexp)
    (car aexp)))

(define operator
  (lambda (aexp)
    (car (cdr aexp))))

;;               The Eight Commandment
;;
;; Use help functions to abstract from representations.

(define sero?
  (lambda (n)
    (null? n)))

(define edd1
  (lambda (n)
    (cons (quote ()) n)))

(define zub1
  (lambda (n)
    (cdr n)))

#;(define o+
  (lambda (n m)
    (cond
     ((sero? m) n)
     (else (edd1 (o+ n (zub1 m)))))))

;;; 7. Friends and Relations

(define member?
  (lambda (s l)
    (cond
     ((null? l) #f)
     ((equal? (car l) s) #t)
     (else (member? s (cdr l))))))

(define set?
  (lambda (lat)
    (cond
     ((null? lat) #t)
     ((member? (car lat) (cdr lat)) #f)
     (else (set? (cdr lat))))))

(define makeset
  (lambda (lat)
    (cond
     ((null? lat) (quote ()))
     ((member? (car lat) (cdr lat)) (makeset (cdr lat)))
     (else (cons (car lat) (makeset (cdr lat)))))))

(define multirember
  (lambda (s l)
    (cond
     ((null? l) (quote ()))
     (else
      (cond
       ((equal? (car l) s) (multirember s (cdr l)))
       (else (cons (car l) (multirember s (cdr l)))))))))

(define makeset
  (lambda (l)
    (cond
     ((null? l) (quote ()))
     (else (cons (car l) (makeset (multirember (car l) (cdr l))))))))

(define subset?
  (lambda (set1 set2)
    (cond
     ((null? set1) #t)
     (else (and (member? (car set1) set2) (subset? (cdr set1) set2))))))

(define eqset?
  (lambda (set1 set2)
    (and (subset? set1 set2)
         (subset? set2 set1))))

(define intersect?
  (lambda (set1 set2)
    (cond
     ((null? set1) #f)
     (else (or (member? (car set1) set2) (intersect? (cdr set1) set2))))))

(define intersect
  (lambda (set1 set2)
    (cond
     ((null? set1) (quote ()))
     ((member? (car set1) set2) (cons (car set1) (intersect (cdr set1) set2)))
     (else (intersect (cdr set1) set2)))))

(define union
  (lambda (set1 set2)
    (cond
     ((null? set1) set2)
     ((member? (car set1) set2) (union (cdr set1) set2))
     (else (cons (car set1) (union (cdr set1) set2))))))

(define intersectall
  (lambda (l-set)
    (cond
     ((null? (cdr l-set) (car l-set))
      (intersect (car l-set) (intersectall (cdr l-set)))))))

(define a-pair?
  (lambda (x)
    (cond
     ((atom? x) #f)
     ((null? x) #f)
     ((null? (cdr x)) #f)
     (else (null? (cdr (cdr x)))))))

(define first
  (lambda (p)
    (car p)))

(define second
  (lambda (p)
    (car (cdr p))))

(define build
  (lambda (s1 s2)
    (cons (s1 (cons s2 (quote ()))))))

(define third
  (lambda (l)
    (car (cdr (cdr l)))))

(define fun?
  (lambda (rel)
    (set? (firsts rel))))

(define revrel
  (lambda (rel)
    (cond
     ((null? rel) (quote ()))
     (else (cons (build (second (car rel)) (first (car rel)))
                 (revrel (cdr rel)))))))

(define revpair
  (lambda (p)
    (build (second p) (first p))))

(define revrel
  (lambda (rel)
    (cond
     ((null? rel) (quote ()))
     (else (cons (revpair (car rel)) (revrel (cdr rel)))))))

(define seconds
  (lambda (l)
    (cond
     ((null? l) (quote ()))
     (cons (cdr (car l) (seconds (cdr l)))))))

(define fullfun?  ; bijective?
  (lambda (rel)
    (set? (seconds rel))))

(define one-to-one?
  (lambda (fun)
    (fun? (revrel fun))))

;;; 8. Lambda the Ultimate

(define rember-f
  (lambda (test? a l)
    (cond
     ((null? l) (quote ()))
     ((test? (car l) a) (cdr l))
     (else (cons (car l) (rember-f test? a (cdr l)))))))

(define eq?-c
  (lambda (a)
    (lambda (x)
      (eq? x a))))

(define eq?-salad (eq?-c (quote salad)))

(define rember-f
  (lambda (test?)
    (lambda (a l)
      (cond
       ((null? l) (quote ()))
       ((test? (car l) a) (cdr l))
       (else (cons (car l) ((rember-f test?) a (cdr l))))))))

(define insertL-f
  (lambda (test?)
    (lambda (new old l)
      (cond
       ((null? l) (quote ()))
       ((test? (car l) old) (cons new l))
       (else (cons (car l) ((insertL-f test?) new old (cdr l))))))))

(define insertR-f
  (lambda (test?)
    (lambda (new old l)
      (cond
       ((null? l) (quote ()))
       ((test? (car l) old) (cons old (cons new (cdr l))))
       (else (cons (car l) ((insertR-f test?) new old (cdr l))))))))

(define seqL
  (lambda (new old l)
    (cons new (cons old l))))

(define seqR
  (lambda (new old l)
    (cons old (cons new l))))

(define insert-g
  (lambda (seq)
    (lambda (new old l)
      (cond
       ((null? l) (quote ()))
       ((eq? (car l) old) (seq new old (cdr l)))
       (else (cons (car l) ((insert-g seq) (cdr l))))))))

(define insertL (insert-g seqL))

(define insertR (insert-g seqR))

(define insertL
  (insert-g
   (lambda (new old l)
     (cons new (cons old l)))))

(define seqS
  (lambda (new old l)
    (cons new l)))

(define subst (insert-g seqS))

(define seqrem
  (lambda (new old l)
    l))

(define rember
  (lambda (a l)
    ((insert-g seqrem) #f a l)))

;;           The Ninth Commandment
;;
;; Abstract common patterns with a new function.

(define ox x)

(define atom-to-function
  (lambda (x)
    (cond
     ((eq? x (quote +)) o+)
     ((eq? x (quote x)) ox)
     (else ^))))

(define value
  (lambda (nexp)
    (cond
     ((atom? nexp) nexp)
     (else ((atom-to-function (operator nexp))
            (value (1st-sub-exp nexp))
            (value (2nd-sub-exp nexp)))))))

(define multirember-f
  (lambda (test?)
    (lambda (a lat)
      (cond
       ((null? lat) (quote ()))
       ((test? a (car lat)) ((multirember-f test?) a (cdr lat)))
       (else (cons (car lat) ((multirember-f test?) a (cdr lat))))))))

(define multirember-eq?
  (multirember-f eq?))

(define eq?-tuna
  (eq?-c (quote tuna)))

(define multiremberT
  (lambda (test? lat)
    (cond
     ((null? lat) (quote ()))
     ((test? (car lat)) (multiremberT test? (cdr lat)))
     (else (cond (car lat) (multiremberT test? (cdr lat)))))))

(define multirember&co
  (lambda (a lat col)
    (cond
     ((null? lat) (col (quote ()) (quote ())))
     ((eq? (car lat) a)
      (multirember&co
       a
       (cdr lat)
       (lambda (newlat seen)
         (col newlat (cons (car lat) seen)))))
     (else
      (multirember&co
       a
       (cdr lat)
       (lambda (newlat seen)
         (col (cons (car lat) newlat) seen)))))))

;;                 The Tenth Commandment
;;
;; Build functions to collect more than one value at a time.

(define multiinsertLR
  (lambda (new oldL oldR lat)
    (cond
     ((null? lat) (quote ()))
     ((eq? (car lat) oldL)
      (cons new (cons oldL (multiinsertLR new oldL oldR (cdr lat)))))
     ((eq? (car lat) oldR)
      (cons oldR (cons new (multiinsertLR new oldL oldR (cdr lat)))))
     (else
      (cons (car lat) (multiinsertLR new oldL oldR (cdr lat)))))))

(define multiinsertLR&co
  (lambda (new oldL oldR lat col)
    (cond
     ((null? lat) (col (quote ()) 0 0))
     ((eq? (car lat) oldL)
      (multiinsertLR&co
       new oldL oldR (cdr lat)
       (lambda (newlat L R)
         (col (cons new (cons oldL newlat))
              (add1 L)
              R))))
     ((eq? (car lat) oldR)
      (multiinsertLR&co
       new oldL oldR (cdr lat)
       (lambda (newlat L R)
         (col (cons oldR (cons new newlat))
              L
              (add1 R)))))
     (else
      (multiinsertLR&co
       new oldL oldR (cdr lat)
       (lambda (newlat L R)
         (col (cons (car lat) newlat)
              L
              R)))))))

(define even?
  (lambda (n)
    (= (ox (/ n 2) 2) n)))

(define evens-only*
  (lambda (l)
    (cond
     ((null? l) (quote ()))
     ((atom? (car l))
      (cond
       ((even? (car l)) (cons (car l) (evens-only* (cdr l))))
       (else (evens-only* (cdr l)))))
     (else
      (cons (evens-only* (car l)) (evens-only* (cdr l)))))))

(define evens-only*&co
  (lambda (l col)
    (cond
     ((null? l) (col (quote ()) 1 0))
     ((atom? (car l))
      (cond
       ((even? (car l))
        (evens-only*&co
         (cdr l)
         (lambda (newl p s)
           (col (cons (car l) newl)
                (ox (car l) p)
                s))))
       (else
        (evens-only*&co
         (cdr l)
         (lambda (newl p s)
           (col newl
                p
                (o+ (car l) s)))))))
     (else
      (evens-only*&co
       (car l)
       (lambda (al ap as)
         (evens-only*&co
          (cdr l)
          (lambda (dl dp ds)
            (col (cons al dl)
                 (ox ap dp)
                 (o+ as ds))))))))))
